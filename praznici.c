#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#define do1 Beep(523.25,300);
#define do12 Beep(523.25,150);
#define do103 Beep(523.25,900);
#define re Beep(587.33,300);
#define re103 Beep(587.33,900);
#define re2 Beep(587.33,150);
#define mi Beep(659.25,300);
#define mi2 Beep(659.25,150);
#define mi103 Beep(659.25,900);
#define fa Beep(698.46,300);
#define fa103 Beep(698.46,900);
#define fa34 Beep(698.46,450);
#define fa2 Beep(698.46,600);
#define sol Beep(783.99,300);
#define sol102 Beep(783.99,600);
#define sol2 Beep(783.99,150);
#define la Beep(880,300);
#define la2 Beep(880,150);
#define la102 Beep(880,600);
#define la103 Beep(880,900);
#define si Beep(932.33,300);
#define si34 Beep(932.33,450);
#define si2 Beep(932.33,150);
#define do2 Beep(1046.50,300);
#define do24 Beep(1046.50,600);
#define do232 Beep(1046.50,450);
#define do22 Beep(1046.50,150);
#define pauza Beep(0,150);
#define re12 Beep(1174.66,300);
#define SIZE 17

void crtajTablu (int tabla [SIZE][SIZE], int counter, int uslov, int di, int dj);
void popuniTablu (int tabla [SIZE][SIZE]);
void melodija (int nota);

int main ()
{
    int mat [SIZE][SIZE];
    //int dj[4]={1,0,-1,0};
    //int di[4]={0,1,0,-1};
    int ik [SIZE];
    int jk [SIZE];
    int i, j, di=1, dj=0;
    char message[]="SRECNI PRAZNICI !";
    int counter=0, uslov=-1;

    //stavlja pocetne koordinate za poruku
    for (j=0; j<SIZE; j++)
    {
        ik[j]=0; jk[j]=j;
    }

    popuniTablu(mat);

    //ubacuje poruku u matricu na osnovu pocetnih koordinata
    for (i=0; i<SIZE; i++)
    {
            mat[ik[i]][jk[i]]=*(message+i);
    }

    crtajTablu(mat, counter, uslov, di, dj);
    //Sleep(3000);
    system("cls");

    i=j=0;

    while (1)
    {
            //Ovaj podesava koordinate poruke

            mat[ik[0]][jk[0]]=' ';
            for (i=1; i<=SIZE-1; i++)
            {
                mat[ik[i]][jk[i]]=*(message+i-1);
            }
            mat[ik[SIZE-1]+di][jk[SIZE-1]+dj]=*(message+i-1);

            //Ovaj for apdejtuje koordinate zmije na tabli

            for (i=0; i<SIZE-1; i++)
            {
                ik[i]=ik[i+1], jk[i]=jk[i+1];
            }
            ik[SIZE-1]=ik[SIZE-1]+di, jk[SIZE-1]=jk[SIZE-1]+dj;

            counter++;

            if (counter%(SIZE-1)==0)
            {
                uslov++;

                switch(uslov%4)
                {
                    case 0: di= 0, dj=-1; break;
                    case 1: di=-1, dj= 0; break;
                    case 2: di= 0, dj= 1; break;
                    case 3: di= 1, dj= 0; break;
                }
            }
            system("cls");
            crtajTablu(mat, counter, uslov, di, dj);
            melodija(counter);
    }

    return 0;
}

void crtajTablu (int tabla [SIZE][SIZE], int counter, int uslov, int di, int dj)
{
    int i, j;
    //printf("COUNTER: %d uslov: %d\n", counter, uslov);
    //printf("di %d dj %d\n", di, dj);
    for (i=0; i<SIZE; i++)
    {
        for (j=0; j<SIZE; j++)
        {
            printf("%c", tabla[i][j]);
        }
        putchar('\n');
    }
}

void popuniTablu (int tabla [SIZE][SIZE])
{
    int i, j;

    for (i=0; i<SIZE; i++)
    {
        for (j=0; j<SIZE; j++)
        {
            tabla[i][j]=' ';
        }
    }
}

void melodija (int nota)
{
    switch ((nota-1)%111+1)
    {
        case 1: do1 break;
        case 2: la break;
        case 3: sol break;
        case 4: fa break;
        case 5: do103 break;
        case 6: do12 break;
        case 7: do12 break;
        case 8: do1 break;
        case 9: la break;
        case 10: sol break;
        case 11: fa break;
        case 12: re103 break;
        case 13: pauza break;
        case 14: re2 break;
        case 15: mi break;
        case 16: si break;
        case 17: la break;
        case 18: sol break;
        case 19: mi103 break;
        case 20: pauza break;
        case 21: mi2 break;
        case 22: do2 break;
        case 23: do2 break;
        case 24: si break;
        case 25: sol break;
        case 26: la103 break;
        case 27: do12 break;
        case 28: do12 break;
        case 29: do1 break;
        case 30: la break;
        case 31: sol break;
        case 32: fa break;
        case 33: do103 break;
        case 34: do12 break;
        case 35: do12 break;
        case 36: do1 break;
        case 37: la break;
        case 38: sol break;
        case 39: fa break;
        case 40: re103 break;
        case 41: pauza break;
        case 42: re2 break;
        case 43: mi break;
        case 44: si break;
        case 45: la break;
        case 46: sol break;
        case 47: do2 break;
        case 48: do2 break;
        case 49: do232 break;
        case 50: do22 break;
        case 51: re12 break;
        case 52: do2 break;
        case 53: si break;
        case 54: sol break;
        case 55: fa2 break;
        case 56: do24 break;
        case 57: la break;
        case 58: la break;
        case 59: la102 break;
        case 60: la break;
        case 61: la break;
        case 62: la102 break;
        case 63: la break;
        case 64: do2 break;
        case 65: fa34 break;
        case 66: sol2 break;
        case 67: la103 break;
        case 68: pauza break;
        case 69: pauza break;
        case 70: si break;
        case 71: si break;
        case 72: si34 break;
        case 73: si2 break;
        case 74: si break;
        case 75: la break;
        case 76: la break;
        case 77: la2 break;
        case 78: la2 break;
        case 79: la break;
        case 80: sol break;
        case 81: sol break;
        case 82: la break;
        case 83: sol102 break;
        case 84: do24 break;
        case 85: la break;
        case 86: la break;
        case 87: la102 break;
        case 88: la break;
        case 89: la break;
        case 90: la102 break;
        case 91: la break;
        case 92: do2 break;
        case 93: fa34 break;
        case 94: sol2 break;
        case 95: la103 break;
        case 96: pauza break;
        case 97: pauza break;
        case 98: si break;
        case 99: si break;
        case 100: si34 break;
        case 101: si2 break;
        case 102: si break;
        case 103: la break;
        case 104: la break;
        case 105: la2 break;
        case 106: la2 break;
        case 107: do2 break;
        case 108: do2 break;
        case 109: si break;
        case 110: sol break;
        case 111: fa103 break;
    }
}
